package assignment.one;

import java.util.Scanner;
import java.lang.*;
public class ProblemFive {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input floating-point number:");
        double number1 = sc.nextDouble();
        
        System.out.println("Input floating-point another number:");
        double number2 = sc.nextDouble();
        
        number1 = Math.round(number1 * 1000);
        System.out.println(number1);
        number1 = number1 / 1000;
        
        number2 = Math.round(number2 * 1000);
        number2 = number2 / 1000;
        
        if(number1 == number2) {
            System.out.println("They are same");
        } else {
            System.out.println("They are different");
        }
        
//        if(Math.abs(number1) == Math.abs(number2)) {
//            System.out.println("They are same");
//        } else {
//            System.out.println("They are different");
//        }
    }
}
