package assignment.one;

import java.util.Scanner;
public class ProblemOne {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int number = sc.nextInt();
        sc.close();
        
        if(number > 0){
            System.out.println("Number is positive");
        } else if(number < 0){
            System.out.println("Number is negative");
        } else {
            System.out.println("Numver is qual to zero");
        }
    }
}
