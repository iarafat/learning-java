package assignment.one;

import java.util.Scanner;
import java.lang.*;
public class ProblemThree {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter the Number:");
        double number = sc.nextDouble();
        
        if(number > 0){
            if(number < 1){
                System.out.println("Number is less then 1");
            } else if(number > 1000000) {
                System.out.println("Number is larger then 1,000,000");
            } else {
                System.out.println("Positive number");
            }
        } else if (number < 0) {
            if(Math.abs(number) < 1) {
                System.out.println("Number is Negative less then 1");
            }else if(Math.abs(number) > 1000000) {
                System.out.println("Number is Negative larger then 1,000,000");
            } else {
                System.out.println("Negative number");
            }
        } else {
            System.out.println("Numver is qual to zero");
        }
    }
}
