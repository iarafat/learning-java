
//package assignment.three;

import java.util.Scanner;
public class ProblemOne {
    public static void main(String[] args) {
        // using temporary or thard variable
        /*int x, y, temp;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter 1st number: ");
        x = sc.nextInt();

        System.out.print("Enter 2nd number: ");
        y = sc.nextInt();
        
        System.out.println("Before Swapping\nx = "+x+"\ny = "+y);
        
        temp = x;
        x = y;
        y = temp;
        
        System.out.println("After Swapping\nx = "+x+"\ny = "+y);*/
        
        // without temporary or thard variable
        int x, y;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter 1st number: ");
        x = sc.nextInt();

        System.out.print("Enter 2nd number: ");
        y = sc.nextInt();
        
        System.out.println("Before Swapping\nx = "+x+"\ny = "+y);
        
        x = x + y;
        y = x - y;
        x = x - y;
        
        System.out.println("After Swapping\nx = "+x+"\ny = "+y);
    }
}
