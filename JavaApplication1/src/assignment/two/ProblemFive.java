package assignment.two;

import java.util.Scanner;
public class ProblemFive {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input the string: ");
        String str = sc.nextLine();
        System.out.println("Number of Words in the string: "+ words(str));
    }
    
    public static int words(String str){
        int count = 0;
        if (!(" ".equals(str.substring(0,1))) || !(" ".equals(str.substring(str.length() - 1))) ){
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == ' '){
                    count++;
                }
            }
            count = count + 1;
        }
        
        return count; // returns 0 if string starts or ends with space " ".
    }
}
