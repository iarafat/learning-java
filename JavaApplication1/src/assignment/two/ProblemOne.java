package assignment.two;

import java.util.Scanner;
public class ProblemOne {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input the 1st number: ");
        double number1 = sc.nextDouble();
        
        System.out.println("Input the 2nd number: ");
        double number2 = sc.nextDouble();
        
        System.out.println("Input the 3rd number: ");
        double number3 = sc.nextDouble();
         System.out.println("The smallest value is "+ smallest(number1,number2,number3));
    }
    
    /*public static double smallest(double number1, double number2, double number3){
        if(number1 < number2 && number1 < number3){
            return number1;
        } else if (number2 < number1 && number2 < number3){
           return number2;
        } else {
            return number3;
        }
    } */
    public static double smallest(double number1, double number2, double number3){
        return Math.min(Math.min(number1, number2), number3);
    }
}
