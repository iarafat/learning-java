package assignment.two;

import java.util.Scanner;
public class ProblemTwo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Input the 1st number: ");
        double number1 = sc.nextDouble();
        
        System.out.println("Input the 2nd number: ");
        double number2 = sc.nextDouble();
        
        System.out.println("Input the 3rd number: ");
        double number3 = sc.nextDouble();
         System.out.println("The average value is "+ average(number1,number2,number3));
    }
    
    public static double average(double number1, double number2, double number3){
        return (number1 + number2 + number3) / 3.0;
    }
}
