package class11task;

import java.io.*;
public class ReadFromFile {
    public static void main(String[] args) throws IOException {
        FileReader freader = new FileReader("Rose\\rose2.txt");
        BufferedReader br = new BufferedReader(freader);
        String s;
        while((s = br.readLine()) != null){
            System.out.println(s);
        }
        freader.close();
    }
}
