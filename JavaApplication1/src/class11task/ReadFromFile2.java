
package class11task;

import java.io.*;
public class ReadFromFile2 {
    public static void main(String[] args) {
        InputStream istream;
        OutputStream ostream;
        int c;
        final int EOF = -1;
        ostream = System.out;
        try {
           File inputFile = new File("Rose\\rose2.txt");
            istream = new FileInputStream(inputFile);
            
            try {
                while((c = istream.read()) != EOF){
                ostream.write(c);
            }
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                try {
                    istream.close();
                    ostream.close();
                } catch (IOException e) {
                    System.out.println("File did not close");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        
    }
}
