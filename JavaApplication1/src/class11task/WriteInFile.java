package class11task;

import java.io.*;
public class WriteInFile {
    public static void main(String[] args) throws IOException {
        File f = new File("Rose\\rose2.txt");
        FileWriter fw = new FileWriter(f, true);
        
        fw.write(97);
        String s = " This is a string line text ";
        fw.write(s);
        char[] ch = {'a', 'V', 'C'};
        fw.write(ch);
        fw.flush();
        fw.close();    
    }
}
