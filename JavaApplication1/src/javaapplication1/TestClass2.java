
package javaapplication1;

public class TestClass2 {
    
    public static void main(String[] args) {
        
      
        int[][] arr =   {
                            {12, 13, 14 },
                            {22, 23, 24 },
                            {32, 33, 34 },
                        };
        
        System.out.println(arr[2][0]);
        /*
        [R0,C0][R0,C1][R0,C2]
        [R1,C0][R1,C1][R1,C2]
        [R2,C0][R2,C1][R2,C2]
        
        response.data.col 
        response.data.row
        
        var arr[response.data.col][response.data.row]

        */
    }
}
