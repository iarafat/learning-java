package javaapplication1;

public class ThemeYellow {
    
    public static void main(String[] args) {
        int j, k, i;
        j = 5;
        k = 6;
        i = j + k;
        
        System.out.println("The addition of "+ j +" and "+ k +" is: " + i +"\n");
        
        System.out.printf("The addition of %d and %d is: %d \n", j, k, i);
    }
    
}